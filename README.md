## assets

Contains all the resource to perform tassk. It contains image of all the bears and .txt file that contains description of the bears.

---

## source

Contains .NET Code and Angular code. This project is built on .NET Core 3.1 using WebAPI, SignalR and ADO.NET.

---
## db

Contains SQL script to create the table the project uses.The database is named BearsClub, However you are free to choose what you like.

---
## Setup

1. Clone the repository **git clone https://RanjeethDevaiah@bitbucket.org/RanjeethDevaiah/bearclub.git**.
2. Go to **bearclub** folder with **cd bearclub**
3. Go to **src** folder with **cd src**
3. Look for file **appsettings.json** to change to connection string to the db
```json
		{
		  "Logging": {
			"LogLevel": {
			  "Default": "Warning"
			}
		  },
		  "ConnectionStrings": {
			"Storage": "Connection string to db" ex:Data Source=localhost;Initial Catalog=BearsClub;Integrated Security=SSPI;
		  },
		  "AllowedHosts": "*"
		}
```
4. Look for file **NLog.config** to change the location of the log file.
```
		<?xml version="1.0" encoding="utf-8" ?>
		<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
			  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			  autoReload="true"
			  internalLogLevel="Info"
			  internalLogFile="c:\temp\internal-nlog.txt">

		  <!-- enable asp.net core layout renderers -->
		  <extensions>
			<add assembly="NLog.Web.AspNetCore"/>
		  </extensions>

		  <!-- the targets to write to -->
		  <targets>
			<!-- write logs to file  -->
			<target xsi:type="File" name="allfile" fileName="Drive and folder \nlog-all-${shortdate}.log" ex:D:\Logs
					layout="${longdate}|${event-properties:item=EventId_Id}|${uppercase:${level}}|${logger}|${message} ${exception:format=tostring}" />

			<!-- another file log, only own logs. Uses some ASP.NET core renderers -->
			<target xsi:type="File" name="ownFile-web" fileName="c:\temp\nlog-own-${shortdate}.log"
					layout="${longdate}|${event-properties:item=EventId_Id}|${uppercase:${level}}|${logger}|${message} ${exception:format=tostring}|url: ${aspnet-request-url}|action: ${aspnet-mvc-action}" />
		  </targets>

		  <!-- rules to map from logger name to target -->
		  <rules>
			<!--All logs, including from Microsoft-->
			<logger name="*" minlevel="Trace" writeTo="allfile" />

			<!--Skip non-critical Microsoft logs and so log only own logs-->
			<logger name="Microsoft.*" maxlevel="Info" final="true" />
			<!-- BlackHole without writeTo -->
			<logger name="*" minlevel="Trace" writeTo="ownFile-web" />
		  </rules>
		</nlog>
```
5. Go to **Properties** folder with **cd Properties**
6. Look for file **launchSettings.json** to change the environment
```json
	{
	  "iisSettings": {
		"windowsAuthentication": false,
		"anonymousAuthentication": true,
		"iisExpress": {
		  "applicationUrl": "http://localhost:50340",
		  "sslPort": 44370
		}
	  },
	  "profiles": {
		"IIS Express": {
		  "commandName": "IISExpress",
		  "launchBrowser": true,
		  "environmentVariables": {
			"ASPNETCORE_ENVIRONMENT": "Production"
		  }
		},
		"BearClub": {
		  "commandName": "Project",
		  "launchBrowser": true,
		  "applicationUrl": "https://localhost:5001;http://localhost:5000",
		  "environmentVariables": {
			"ASPNETCORE_ENVIRONMENT": "Production"
		  }
		}
	  }
	}
```
---
## How to run

1. Download [.NET 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) from official Microsoft site.
2. Install angular cli **npm install -g @angular/cli**.
3. Go to **ClientApp** folder with **cd ClientApp** 
4. Run command **npm install**.
5. Run command **ng build --aot**.
6. Go back to **src** folder with **cd ..** 
7. Run command **dotnet run**.

---
## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

