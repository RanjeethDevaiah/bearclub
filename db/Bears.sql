﻿CREATE TABLE [dbo].[Bears](
	[id] [uniqueidentifier] NOT NULL,
	[type] [nvarchar](100) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[imageId] [uniqueidentifier] NULL,
 CONSTRAINT [PK_Bears] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Bears] ADD  CONSTRAINT [DF_Bears_id]  DEFAULT (newsequentialid()) FOR [id]
GO
