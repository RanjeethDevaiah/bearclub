﻿using BearClub.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BearClub.Interfaces
{
    public interface IBearsRepository
    {
        Task<ICollection<Bear>> GetAllBears();
        Task<Bear> GetBear(Guid id);
        Task<Bear> AddBear(Bear bear);
        Task<int> UpdateBear(Bear bear);
        Task<int> DeleteBear(Guid id);
    }
}
