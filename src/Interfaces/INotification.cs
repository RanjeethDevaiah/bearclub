﻿using BearClub.DTO;
using BearClub.Enums;
using System;
using System.Threading.Tasks;

namespace BearClub.Interfaces
{
    public interface INotification
    {
        Task Notify(Guid id,NotificationEvent eventType,Bear bear);
    }
}
