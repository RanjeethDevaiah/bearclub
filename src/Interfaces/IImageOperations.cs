﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace BearClub.Interfaces
{
    public interface IImageOperations
    {
        Task<MemoryStream> GetImage(Guid? id);
        Task DeleteImage(Guid? id);
        Task<string> SaveImage(Stream stream);        
        string Extension { get; }
    }
}
