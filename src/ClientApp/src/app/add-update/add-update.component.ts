import { Component, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStore, NotificationService } from '../providers';
import { IBear, INotification } from '../interface';
import { NotificationEvent } from '../enums';
import { FormGroup, FormControl } from '@angular/forms';
import { mergeMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import * as Editor from '@ckeditor/ckeditor5-build-decoupled-document';


@Component({
  selector: 'add-update',
  templateUrl: 'add-update.component.html',
  styleUrls: ['add-update.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddUpdateComponent implements OnDestroy {
  public editor = Editor;
  private routeType: string;
  private selectedFile: File;
  private subscription$: Subscription;

  form: FormGroup = new FormGroup({
    title: new FormControl(),
    description: new FormControl(),
    id: new FormControl(),
    img: new FormControl()
  });

  action: string;
  type: string;
  url: string;
  uploading: boolean = false;

  config: Object = {
    removePlugins: ['Link', 'Image', 'ImageToolbar', 'ImageCaption', 'ImageStyle', 'ImageTextAlternative', 'ImageUpload', 'ImageResize', 'MediaEmbed']
  }

  alert: IAlert = {
    type: "collapse",
    description: "collapse"
  }

  constructor(private route: ActivatedRoute, private dataStore: DataStore,
    private router: Router, notification: NotificationService) {

    this.routeType = this.route.snapshot.url[0].path;

    if (this.routeType.toLocaleLowerCase() === 'add') {
      this.action = "Add";
      this.type = "Bear";
    }
    else {
      this.route.queryParams
        .pipe(mergeMap(params => this.dataStore.getBear(params["id"])))
        .subscribe((data: IBear) => {

          this.subscription$ = notification.onDataChange().subscribe((event: INotification) => {

            if (event.eventType !== NotificationEvent.Insert && data.id === event.id) {

              if (event.eventType === NotificationEvent.Update) {

                this.onLoadData(event.bear);
              }
              else {
                this.router.navigate(["/"]);
              }

            }

          });

          this.onLoadData(data);

        }, error => {

          if (error.status === 404) {
            this.router.navigate(["/"]);
          }
        });
    }
  }


  private onLoadData(data: IBear) {
    this.form = new FormGroup({
      title: new FormControl(data.title),
      description: new FormControl(data.description),
      id: new FormControl(data.id),
      img: new FormControl(data.img)
    });

    this.action = "Edit";
    this.type = data.title;

    if (data.img) {
      this.url = `api/v1/image/${data.img}`;
    }
  }

  onSelected(file: File) {
    this.selectedFile = file;
  }

  onUpload(data: IBear) {
    var ret = false;

    if (!data.title) {
      this.alert.type = "";
      ret = true;
    }



    if (!data.description) {
      this.alert.description = ""
      ret = true
    }

    if (ret) {
      return;
    }

    this.alert.type = "collapse";
    this.alert.description = "collapse";

    if (this.routeType.toLocaleLowerCase() === 'add') {

      this.uploading = true;

      this.dataStore.addBear(data, this.selectedFile)
        .subscribe(() => {
          this.form = new FormGroup({
            title: new FormControl(),
            description: new FormControl(),
            id: new FormControl(),
            img: new FormControl()
          });

          this.url = null;
          this.selectedFile = null;
          this.uploading = false;
        });
    }
    else {
      this.uploading = true;
      this.dataStore.updateBear(data, this.selectedFile)
        .subscribe(() => {
          this.uploading = false;
          this.type = data.title;
        });

    }
  }

  onReady(editor) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}

//Private interface
interface IAlert {
  type: string,
  description: string
}
