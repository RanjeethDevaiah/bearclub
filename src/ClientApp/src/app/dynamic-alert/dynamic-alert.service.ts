import {
  Injectable,
  ComponentFactoryResolver,
  ApplicationRef,
  Injector,
  EmbeddedViewRef,
  ComponentRef,
  Type
} from '@angular/core';

import { DynamicAlertComponent } from './dynamic-alert/dynamic-alert.component';
import { DynamicAlertModule } from './dynamic-alert.module';

@Injectable({
  providedIn: DynamicAlertModule
})
export class DynamicAlertService {

  private componentRef: ComponentRef<DynamicAlertComponent>

  public componentInstance: any;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector:Injector
  ) { }

  private appendComponentToBody() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(DynamicAlertComponent);
    const componentRef = componentFactory.create(this.injector);

    this.appRef.attachView(componentRef.hostView);

    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;

    document.body.appendChild(domElem);

    this.componentRef = componentRef;
  }

  private removeComponentFromBody() {
    this.componentRef.destroy();
  }

  public open(componentType: Type<any>) {
    this.appendComponentToBody();

    this.componentRef.instance.loadType(componentType);    
    this.componentInstance = this.componentRef.instance.componentInstance;
  }

  public close() {
    this.removeComponentFromBody();
  }
}
