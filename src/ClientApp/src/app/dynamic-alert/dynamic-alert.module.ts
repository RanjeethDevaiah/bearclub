import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicAlertComponent } from './dynamic-alert/dynamic-alert.component';
import { InsertionDirective } from './insertion.directive';



@NgModule({
  declarations: [DynamicAlertComponent, InsertionDirective],
  imports: [
    CommonModule
  ],
  entryComponents: [DynamicAlertComponent]
})
export class DynamicAlertModule { }
