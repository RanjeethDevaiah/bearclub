import { Component, ViewChild, Type, OnDestroy, AfterViewInit, ComponentFactoryResolver, ComponentRef, ChangeDetectorRef, Injector } from '@angular/core';

import { InsertionDirective } from '../insertion.directive';

@Component({
  selector: 'dynamic-alert',
  templateUrl: './dynamic-alert.component.html',
  styleUrls: ['./dynamic-alert.component.css']
})
export class DynamicAlertComponent implements AfterViewInit, OnDestroy {    

  private componentRef: ComponentRef<any>;  
  public componentInstance: any;

  @ViewChild(InsertionDirective) insertionPoint: InsertionDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver, private cd: ChangeDetectorRef, private inject:Injector) { }

  loadType(componentType: Type<any>) {
    let componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    this.componentRef = componentFactory.create(this.inject);
    this.componentInstance = this.componentRef.instance;
  }

  loadChildComponent() {   

    let viewContainerRef = this.insertionPoint.viewContainerRef;
    viewContainerRef.clear();   
    viewContainerRef.insert(this.componentRef.hostView);    
  }

  ngOnDestroy(): void {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }
  ngAfterViewInit(): void {
    this.loadChildComponent();
    this.cd.detectChanges();   
  }

  onOverlayClicked(evt: MouseEvent) {
    // close the dialog
  }

  onDialogClicked(evt: MouseEvent) {
    evt.stopPropagation()
  }
}
