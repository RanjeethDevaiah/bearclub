import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit { 

  show: boolean = false;

  constructor(private router: Router, private authService: AuthenticationService) {
    
  }

  ngOnInit() {
    console.log(this.router);
  }

  login() {    
    this.authService.login(this.router.url || '/home');
  }

  logout() {
    
    this.authService.logout(this.router.url || '/home');
  }

  authenticated(): boolean {    
    return this.authService.isAuthenticated();
  }

  getUserName(): string {
    
    if (this.authService.isAuthenticated()) {      

      return this.authService.getUser().profile.name;
    }
    else {
      return "Not signed in...";
    }
  }
}
