import { Injectable } from '@angular/core';
import { UserManager, UserManagerSettings, User } from 'oidc-client';
import { BehaviorSubject, Observable } from 'rxjs'; 


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private settings: UserManagerSettings = {
    authority: 'https://localhost:5001',
    client_id: 'spa',
    redirect_uri: 'https://localhost:5004/auth-callback',
    post_logout_redirect_uri: 'https://localhost:5004/logout',
    response_type: "code",
    scope: "openid profile api1",    
  };

  private manager = new UserManager(this.settings);
  private user: User | null;
  private signoutUrl: string;

  private _authBehaviorSubject = new BehaviorSubject(false);
  private _authStatusObservable = this._authBehaviorSubject.asObservable();

  constructor() {
    this.manager.getUser().then(user => {
      this.user = user;
      this._authBehaviorSubject.next(this.isAuthenticated());
    });
  }

  login(returnUrl?: string): Promise<void> {
    this.signoutUrl = null;

    return this.manager.signinRedirect({
      state: { //your params go here
        returnUrl: returnUrl
      }
    });
  }

  async completeAuthentication() {
    this.user = await this.manager.signinRedirectCallback();

    this._authBehaviorSubject.next(this.isAuthenticated());
  }

  async completeLogout() {
    await this.manager.signoutCallback();

    this._authBehaviorSubject.next(this.isAuthenticated());
  }

  isAuthenticated(): boolean {
    return this.user != null && !this.user.expired;
  }

  async logout(returnUrl?: string): Promise<void> {

    this.signoutUrl = returnUrl;
    return this.manager.signoutRedirect();
  }

  getSignOutUrl(): string {
    return this.signoutUrl;
  }

  getAuthenticationEvent(): Observable<boolean> {
    return this._authStatusObservable;
  }

  getUser(): User {
    return this.user;
  }
}
