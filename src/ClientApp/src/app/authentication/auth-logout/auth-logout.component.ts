import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../providers/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-auth-logout',
  templateUrl: './auth-logout.component.html',
  styleUrls: ['./auth-logout.component.css']
})
export class AuthLogoutComponent implements OnInit {

  constructor(private authService: AuthenticationService, private router: Router, private route: ActivatedRoute) { }

  async ngOnInit() {
    await this.authService.completeLogout();

   setTimeout(() => {  
      this.router.navigate([this.authService.getSignOutUrl() || '/home']); 
    }, 2000);    
  }

}
