import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationRoutingModule } from './authentication-routing.module'
import { AuthenticationService } from './providers/authentication.service';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { AuthLogoutComponent } from './auth-logout/auth-logout.component'

@NgModule({
  declarations: [
    AuthCallbackComponent,
    AuthLogoutComponent],
  imports: [
    CommonModule,
    AuthenticationRoutingModule
  ],
  providers: [AuthenticationService]
})
export class AuthenticationModule { }
