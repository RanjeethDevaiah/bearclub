import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthCallbackComponent } from './auth-callback/auth-callback.component';
import { AuthLogoutComponent } from './auth-logout/auth-logout.component';

const routes: Routes = [ 
  { path: 'auth-callback', component: AuthCallbackComponent },
  { path: 'logout', component: AuthLogoutComponent}
];

@NgModule({  
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthenticationRoutingModule {}
