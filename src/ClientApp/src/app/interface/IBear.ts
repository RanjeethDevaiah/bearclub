export interface IBear {
    id?: string;
    img?: string;
    title: string;
    description:string
}
