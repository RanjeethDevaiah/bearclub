import { NotificationEvent } from '../enums';
import { IBear } from '../interface';

export interface INotification {
  id: string,
  eventType: NotificationEvent,
  bear?: IBear
}
