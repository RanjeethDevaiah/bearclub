"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var NotificationEvent;
(function (NotificationEvent) {
    NotificationEvent[NotificationEvent["Insert"] = 0] = "Insert";
    NotificationEvent[NotificationEvent["Update"] = 1] = "Update";
    NotificationEvent[NotificationEvent["Delete"] = 2] = "Delete";
})(NotificationEvent = exports.NotificationEvent || (exports.NotificationEvent = {}));
//# sourceMappingURL=NotificationEvent.js.map