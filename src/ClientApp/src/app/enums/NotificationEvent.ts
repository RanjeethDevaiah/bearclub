export enum NotificationEvent {
  Insert = 0,
  Update = 1,
  Delete = 2
}
