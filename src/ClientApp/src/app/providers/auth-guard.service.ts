import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot  } from '@angular/router';
import { AuthenticationService } from '../authentication';
import { AlertService } from './alert.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate,CanActivateChild {

  

  constructor(private authService: AuthenticationService, private router: Router,private alert:AlertService) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    return this.isAuthenticated(state);    
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    return this.isAuthenticated(state);
  }

  private isAuthenticated(state: RouterStateSnapshot) {

    if (this.authService.isAuthenticated())
      return true;    

    this.alert.openErrorAlert("This operation needs user authentication.");

    this.router.navigate(['./home'], { queryParams: { redirect: state.url }, replaceUrl: true });

    return false;
  }
}
