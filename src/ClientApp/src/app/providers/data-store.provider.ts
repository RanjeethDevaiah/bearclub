import { Observable } from "rxjs"
import { map,mergeMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { IBear, IFile } from '../interface'
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class DataStore {

    private apiBaseUri: string = "api/v1";

    constructor(private http: HttpClient) {

    }

    getBears(): Observable<IBear[]> {

        return this.http.get<IBear[]>(`${this.apiBaseUri}/bears`);     
    }

    getBear(id:string): Observable<IBear> {

        return this.http.get<IBear>(`${this.apiBaseUri}/bears/${id}`);
    }

    addBear(bear: IBear, image: File): Observable<IBear> {

        if (!image) {
            return this.http.post<IBear>(`${this.apiBaseUri}/bears`, bear);                   
        }
        else {
            const fileUpload = new FormData();
            fileUpload.append('file', image, image.name);
            return this.http.post(`${this.apiBaseUri}/image`, fileUpload)
                .pipe<IBear>(map((result: IFile) => {
                    bear.img = result.fileName;
                    return bear;
                }))
                .pipe(mergeMap((result: IBear) => this.http.post<IBear>(`${this.apiBaseUri}/bears`, result)))
                .pipe(map((result: IBear) => result));   
        }
                 
    }

    updateBear(bear: IBear, image: File): Observable<number> {

        if (!image) {
            return this.http.put<number>(`${this.apiBaseUri}/bears`, bear);               
        }
        else {
            const fileUpload = new FormData();
            fileUpload.append('file', image, image.name);
            return this.http.post(`${this.apiBaseUri}/image`, fileUpload)
                .pipe<IBear>(map((result: IFile) => {
                    bear.img = result.fileName;
                    return bear;
                }))
                .pipe(mergeMap((result: IBear) => this.http.put<number>(`${this.apiBaseUri}/bears`, result)))
                .pipe(map((result: number) => result));
        }
    }

    deleteBear(id: string): Observable<number> {

        return this.http.delete<number>(`${this.apiBaseUri}/bears/${id}`);
    }
}
