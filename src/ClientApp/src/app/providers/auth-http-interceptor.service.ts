import { Injectable } from '@angular/core';
import { AuthenticationService } from '../authentication';
import { AlertService } from './alert.service';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError,EMPTY } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthHttpInterceptorService implements HttpInterceptor {

  constructor(private auth: AuthenticationService, private errorDialog: AlertService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.auth.isAuthenticated()) {
      req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + this.auth.getUser().access_token) });

      return next.handle(req).pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
                      
            
          }
          return event;
        }),
        catchError((error: HttpErrorResponse) => {
          
          this.errorDialog.openHttpErrorDialog(error.status, error && error.error.reason ? error.error.reason : '');
          //return throwError(error);

          return EMPTY;
        })
      );
    }
    else {      
      //throw new Error("The user is not authenticated.");
      this.errorDialog.openErrorAlert("The user is not authenticated.");
      return EMPTY;
    }
  }
}
