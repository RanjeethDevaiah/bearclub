import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorDialogComponent } from '../dialogs';
import { ErrorAlertComponent } from '../error-alert/error-alert.component';
import { DynamicAlertService } from '../dynamic-alert';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private modalService: NgbModal, private errorAlert: DynamicAlertService) { }

  openHttpErrorDialog(code: number, reason: string) {
    const modalRef = this.modalService.open(HttpErrorDialogComponent);
    modalRef.componentInstance.code = code;
    modalRef.componentInstance.reason = reason;
  }

  openErrorAlert(message: string) {

    this.errorAlert.open(ErrorAlertComponent);
    this.errorAlert.componentInstance.alert = 'danger';
    this.errorAlert.componentInstance.message = message;
    setTimeout(() => { this.errorAlert.close(); }, 3000);
  }
}
