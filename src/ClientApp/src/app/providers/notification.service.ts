import { Injectable } from '@angular/core';
import { Subject, Observable } from "rxjs";
import { HubConnectionBuilder, HubConnection } from "@microsoft/signalr";
import { NotificationEvent } from '../enums';
import { IBear, INotification } from '../interface';
import { AuthenticationService } from '../authentication';


@Injectable({
  providedIn: 'root'
})

export class NotificationService {
  private connection: HubConnection;
  private subject: Subject<INotification> = new Subject<INotification>();

  constructor(auth: AuthenticationService) {

    auth.getAuthenticationEvent().subscribe((isAuthenticated: boolean) => {

      if (isAuthenticated) {
        this.connection = new HubConnectionBuilder()
          .withUrl("/NotificationHub", { accessTokenFactory: () => auth.getUser().access_token })
          .build();

        this.connection.on("DataChanged", (id: string, eventTye: NotificationEvent, bear?: IBear) => {
          this.subject.next({
            id: id,
            eventType: eventTye,
            bear: bear
          });
        });

        this.connection.start();

      }

      else {
        if (this.connection) {
          this.connection.stop();
        }
      }
    });
  }

  onDataChange(): Observable<INotification> {
    return this.subject;
  }
}
