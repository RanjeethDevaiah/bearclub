import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AuthenticationModule } from './authentication';
import { DynamicAlertModule } from './dynamic-alert';

import { ImageCropperModule } from 'ngx-image-cropper';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { ReadComponent } from './read/read.component';
import { AddUpdateComponent } from './add-update/add-update.component';
import { AddUpdateImageComponent } from './add-update-img/add-update-img.component';
import { LoginComponent } from './login/login.component';

import { DataStore, NotificationService, AlertService, AuthHttpInterceptorService } from "./providers"
import { PagesPipe, PagesContentPipe } from "./pipes"

import { CKEditorModule } from '@ckeditor/ckeditor5-angular';



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ReadComponent,
    AddUpdateComponent,
    AddUpdateImageComponent,
    PagesPipe,
    PagesContentPipe,
    LoginComponent    
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    DynamicAlertModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    ImageCropperModule,
    AuthenticationModule,
    AppRoutingModule],
  providers: [
    DataStore,
    NotificationService,    
    AlertService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptorService, multi: true }],    
  bootstrap: [AppComponent]
})
export class AppModule { }
