import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ReadComponent } from './read/read.component';
import { AddUpdateComponent } from './add-update/add-update.component';
import { AuthGuardService } from './providers';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'add', component: AddUpdateComponent, canActivate: [AuthGuardService] },
  { path: 'edit', component: AddUpdateComponent, canActivate: [AuthGuardService] },
  { path: 'read', component: ReadComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  declarations: [],
  imports: [    
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule]
})
export class AppRoutingModule { }
