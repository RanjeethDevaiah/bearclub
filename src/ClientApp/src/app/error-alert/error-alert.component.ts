import { Component,Input } from '@angular/core';

@Component({
  selector: 'app-example',
  templateUrl: './error-alert.component.html',
  styleUrls: ['./error-alert.component.css']
})
export class ErrorAlertComponent {  

  alerts: Object = {
    "success": "alert-success",
    "danger": "alert-danger",
    "warning": "alert-warning",
    "info":"alert-info"
  };

  @Input() alert;
  @Input() message;
}
