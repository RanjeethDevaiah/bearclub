import { Component, Input, Output, EventEmitter, ViewChild, AfterViewChecked } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
    selector: "add-update-img",
    templateUrl: "add-update-img.component.html",
    styleUrls: ['add-update-img.component.css']
})

export class AddUpdateImageComponent implements AfterViewChecked {

    @ViewChild("img", { static: false }) image: any;

    uri: string;
    cropped: boolean = true;
    imageChangedEvent: any = '';    

    @Input("default") default: string;

    @Input("src")
    set imageUrl(uri: string) {
        this.uri = uri;
        this.cropped = true;
    }

    @Output()
    selected: EventEmitter<Blob> = new EventEmitter<Blob>();

    onFileChanged(event) {

        this.imageChangedEvent = event;
        this.cropped = false;
    }

    imageCropped(event: ImageCroppedEvent) {
        this.uri = event.base64;
        this.selected.emit(event.file);
    }

    ngAfterViewChecked() {

        if (this.image) {
            if (this.uri) {
                this.image.nativeElement.src = this.uri;
            }
            else {
                this.image.nativeElement.src = this.default;
            }

            this.image.nativeElement.onerror = `this.onerror=null;this.src='${this.default}';`;
        }
    }

    onCropped() {
        this.cropped = true;
    }
}
