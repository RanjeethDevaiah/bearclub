import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';


@Pipe({ name: "pages" })

export class PagesPipe implements PipeTransform {
    transform(array: any[], itemsOnPage: number): number[] {
        let range = Math.ceil(array.length / itemsOnPage);
        let pages = _.range(range);
        return pages;
    }
}
