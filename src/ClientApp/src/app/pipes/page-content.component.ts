import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: "pageContent" })

export class PagesContentPipe implements PipeTransform {
    transform(array: any[], pageNumber: number, items: number): any[] {
        let sub = array.slice(pageNumber, pageNumber + items);
        return sub;
    }
}
