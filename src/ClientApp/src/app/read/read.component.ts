import { Component,OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataStore, NotificationService } from '../providers';
import { IBear, INotification } from '../interface';
import { Subscription } from 'rxjs';
import { NotificationEvent } from '../enums';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'read',
  templateUrl: './read.component.html'
})
export class ReadComponent implements OnDestroy {

  bear: IBear = {
    title: "",
    description: "",
    img: 'NoImage.png'
  };
  private subscription$: Subscription;

  constructor(route: ActivatedRoute, router: Router, dataStore: DataStore, notification: NotificationService) {
    route.queryParams
      .pipe(mergeMap(params => dataStore.getBear(params["id"])))
      .subscribe((data: IBear) => {

        this.subscription$ = notification.onDataChange().subscribe((event: INotification) => {

          if (event.eventType !== NotificationEvent.Insert && data.id === event.id) {

            if (event.eventType === NotificationEvent.Update) {

              this.onLoad(event.bear);
            }
            else {
              router.navigate(["/"]);
            }

          }

        });

        this.onLoad(data);      

      }, error => {

        if (error.status === 404) {
          router.navigate(["/"]);
        }

      });
  }

  onLoad(data: IBear) {

    this.bear = data;

    if (data.img) {
      this.bear.img = `api/v1/image/${data.img}`;
    }
    else {
      this.bear.img = 'NoImage.png';
    }
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }
}
