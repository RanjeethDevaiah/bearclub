import { Component, OnDestroy } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { DataStore, NotificationService } from '../providers'
import { IBear } from '../interface';
import { error } from '@angular/compiler/src/util';
import { mergeMap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../authentication';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls:['home.component.css']
})
export class HomeComponent implements OnDestroy {

  bears: IBear[] = [];
  subscription$: Subscription;
  

    constructor(private route: Router, private store: DataStore, notification: NotificationService, auth:AuthenticationService) {

      auth.getAuthenticationEvent().subscribe((isAuthenticated: boolean) => {        

        if (isAuthenticated) {
          store.getBears().subscribe(data => this.bears = data);          
        }

      });
        

        this.subscription$ = notification.onDataChange().subscribe(() => {
              //Insert/Update/Delete might have occured. Either check cached data or retreive again
            store.getBears().subscribe(data => this.bears = data);
        });
    }

    onAdd() {
        this.route.navigate(['/add']);
    }


    read(id: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: { 'id': id }
        };
        this.route.navigate(['/read'], navigationExtras);
    }


    update(id: string) {
        let navigationExtras: NavigationExtras = {
            queryParams: { 'id': id }           
        };
        this.route.navigate(['/edit'],navigationExtras);
    }

    delete(id: string) {
        this.store.deleteBear(id)
            .pipe(mergeMap((record: number) => this.store.getBears()))
            .subscribe(result => {
                this.bears = result;               
            });
    }

    getText(description) {        

        var temporalDivElement = document.createElement("div");
        // Set the HTML content with the providen
        temporalDivElement.innerHTML =description;
        // Retrieve the text property of the element (cross-browser support)
        return temporalDivElement.textContent || temporalDivElement.innerText || "";
    }

    ngOnDestroy() {
        this.subscription$.unsubscribe();
    }
}
