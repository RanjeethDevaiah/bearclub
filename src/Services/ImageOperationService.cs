﻿using BearClub.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;

namespace BearClub.Services
{
    public class ImageOperationService: IImageOperations
    {
        private readonly string _folder = "uploads";
        private readonly IWebHostEnvironment _env;
        private readonly ILogger<ImageOperationService> _logger;

        public ImageOperationService(IWebHostEnvironment env, ILogger<ImageOperationService> logger)
        {
            _env = env;
            _logger = logger;
        }

        public string Extension
        {
            get
            {
                return "png";
            }
        }

        public async Task<MemoryStream> GetImage(Guid? id)
        {
            if (!id.HasValue)
                return null;

            var path = Path.Combine(_env.WebRootPath, _folder);
            path = Path.Combine(path, $"{id.Value.ToString("N")}.{Extension}");

            if (!File.Exists(path))
                return null;            

            using (var file = File.OpenRead(path))
            {
                var memory = new MemoryStream();
                await file.CopyToAsync(memory);
                memory.Position = 0;
                return memory;
            }            
        }

        public Task DeleteImage(Guid? id)
        {
            if (!id.HasValue)
                return Task.CompletedTask;


            return Task.Run(() =>
            {
                var path = Path.Combine(_env.WebRootPath, "uploads");
                path = Path.Combine(path, $"{id.Value.ToString("N")}.{Extension}");

                if (File.Exists(path))
                {
                    try
                    {
                       File.Delete(path);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Exception {ex}");

                        //Donot throw exception if you cannot delete the file.
                    }
                }
            });
        }
        public async Task<string> SaveImage(Stream file)
        {
            var path = Path.Combine(_env.WebRootPath, "uploads");
            Directory.CreateDirectory(path);

            var fileName = $"{Guid.NewGuid().ToString("N")}";

            path = Path.Combine(path, $"{fileName}.{Extension}");

            using (var stream = File.Create(path))
            {
                await file.CopyToAsync(stream);
            }

            return fileName;            
        }
    }
}
