﻿using BearClub.DTO;
using BearClub.Enums;
using BearClub.Hubs;
using BearClub.Interfaces;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace BearClub.Services
{
    public class HubNotificationService : INotification
    {
        private readonly IHubContext<NotificationHub> _hub;

        public HubNotificationService(IHubContext<NotificationHub> hub)
        {
            _hub = hub;
        }
        public async Task Notify(Guid id, NotificationEvent eventType, Bear bear)
        {
            await _hub.Clients.All.SendAsync("DataChanged", id,(int) eventType, bear);
        }
    }
}
