﻿using System;

namespace BearClub.DTO
{
    public class Error
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
    }
}
