﻿using Newtonsoft.Json;
using System;

namespace BearClub.DTO
{
    public class Bear
    {
        public Guid? Id { get; set; }

        [JsonProperty("img")]
        public Guid? ImageId { get; set; }
        [JsonProperty("title")]
        public string Type { get; set; }
        public string Description { get; set; }
    }
}
