﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text;

namespace BearClub.Filters
{
    public class ValidateModelStateFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            //Donothing
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var modelState = context.ModelState;

            if (!modelState.IsValid)
            {
                var errors = new StringBuilder();
                
                foreach(var key in modelState.Keys)
                {
                    foreach(var error in modelState[key].Errors)
                    {
                        errors.AppendLine(error.ErrorMessage);
                    }
                }

                context.Result = new BadRequestObjectResult(errors.ToString());
            }
        }
    }
}
