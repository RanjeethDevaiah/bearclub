﻿using BearClub.Extensions;
using BearClub.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace BearClub.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class ImageController : ControllerBase
    {        
        private readonly ILogger<ImageController> _logger;
        private readonly IImageOperations _image;
        public ImageController(ILogger<ImageController> logger, IImageOperations image)
        {           
            _logger = logger;
            _image = image;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFile(Guid id)
        {        
            try
            {
                var memory = await _image.GetImage(id);

                if(memory == null)
                {
                    return this.HandleClientError(404, _logger, $"Image {id} not found");
                }

                return File(memory, $"image/{_image.Extension}");
            }
            catch(Exception ex)
            {
                return this.HandleException(_logger, ex);
            }
        }

        [HttpPost]
        public async Task<ActionResult> FileUpload(IFormFile file)
        {
            if(file.Length > 0)
            {               

                try
                {

                    var fileName = await _image.SaveImage(file.OpenReadStream());

                    return Ok(new { fileName });
                }
                catch(Exception ex)
                {
                    return this.HandleException(_logger, ex);
                }
            }

            return this.HandleClientError(400, _logger, "File with zero content");
        }       
    }
}