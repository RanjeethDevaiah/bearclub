﻿using BearClub.DTO;
using BearClub.Enums;
using BearClub.Exceptions;
using BearClub.Extensions;
using BearClub.Interfaces;
using BearClub.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace BearClub.Controllers
{
    [ServiceFilter(typeof(ValidateModelStateFilter))]
    [Route("api/v1/[controller]")]
    [ApiController]
    [Authorize]
    public class BearsController : ControllerBase
    {
        private readonly IBearsRepository _repository;
        private readonly ILogger<BearsController> _logger;
        private readonly IImageOperations _image;
        private readonly INotification _notification;
        public BearsController(IBearsRepository repository, ILogger<BearsController> logger, 
            IImageOperations image, INotification notification)
        {
            _logger = logger;
            _repository = repository;
            _image = image;
            _notification = notification;
        }

        [HttpGet]
        public async Task<ActionResult> GetAllBears()
        {
            try
            {                
                return Ok(await _repository.GetAllBears());
            }
            catch(Exception ex)
            {
                return this.HandleException(_logger, ex);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetABear(Guid id)
        {
            try
            {
                return Ok(await _repository.GetBear(id));
            }
            catch(RecordNotFoundException ex)
            {
                return this.HandleClientError(404,_logger, ex.Message);
            }
            catch (Exception ex)
            {
                return this.HandleException(_logger, ex);
            }
        }

        [HttpPost]
        public async Task<ActionResult> InsertBear(Bear bear)
        {
            try
            {
                var inserted = await _repository.AddBear(bear);

                await _notification.Notify(inserted.Id.Value, NotificationEvent.Insert, inserted);

                return Ok(inserted);
            }
            catch (Exception ex)
            {
                return this.HandleException(_logger, ex);
            }
        }

        [HttpPut()]
        public async Task<ActionResult> UpdateBear(Bear bear)
        {
            try
            {
                try
                {
                    var previousBear = await _repository.GetBear(bear.Id.Value);

                    if(bear.ImageId != previousBear.ImageId)
                    {
                        await _image.DeleteImage(previousBear.ImageId);
                    }                    
                    var updates = await _repository.UpdateBear(bear);

                    await _notification.Notify(bear.Id.Value, NotificationEvent.Update, bear);

                    return Ok(new { updates });
                }
                catch(RecordNotFoundException ex)
                {
                    return this.HandleClientError(404, _logger, ex.Message);
                }  
            }
            catch (Exception ex)
            {
                return this.HandleException(_logger, ex);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteBear(Guid id)
        {
            try
            {
                try
                {
                    var bear = await _repository.GetBear(id);
                    var deletes = await _repository.DeleteBear(id);
                    await _image.DeleteImage(bear.ImageId);

                    await _notification.Notify(id, NotificationEvent.Delete, null);

                    return Ok(new { deletes });
                }
                catch(RecordNotFoundException ex)
                {
                    this.HandleClientError(404, _logger, ex.Message);
                }                

                return this.HandleClientError(404, _logger, "Resource for the id not found.");
            }
            catch (Exception ex)
            {
                return this.HandleException(_logger, ex);
            }
        }
    }
}