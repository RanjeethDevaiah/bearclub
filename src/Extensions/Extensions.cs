﻿using BearClub.DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace BearClub.Extensions
{
    public static class Extensions
    {
        public static ObjectResult HandleException<T>(this T controller,ILogger<T> logger,Exception ex) where T:ControllerBase
        {
            var expId = Guid.NewGuid();
            logger.LogError(ex,$"[{expId}] :: Something went wrong in the server.");
            return controller.StatusCode(500, new Error { Id = expId, Message = "Something went wrong in the server." });
        }

        public static ObjectResult HandleClientError<T>(this T controller,int code, ILogger<T> logger, string message) where T : ControllerBase
        {
            var expId = Guid.NewGuid();
            logger.LogWarning($"[{expId}] :: Client error code {code} {message}.");
            return controller.StatusCode(code, new Error { Id = expId, Message = message });
        }
    }
}
