﻿using BearClub.Exceptions;
using Pluralize.NET.Core;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BearClub.Repository
{
    /// <summary>
    /// This repository assumes that the type name is same as the table name and primary key is always called id.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AbstractRepository<T> where T:class
    {
        private readonly string _connectionString;
        static AbstractRepository()
        {
            var type = typeof(T);
            var propertyInfos = type.GetProperties();
            var list = new List<PropertyInfo>();

            TableName = new Pluralizer().Pluralize(type.Name);

            foreach (var info in propertyInfos)
            {
                if(info.Name.Equals("Id",StringComparison.InvariantCultureIgnoreCase))
                {
                    PrimaryKey = info;
                }
                else
                {
                    list.Add(info);
                }
            }

            Others = list.ToArray();

            CreateSelectAll();
            CreateInsert();
            CreateUpdate();
            CreatedDelete();
        }

        public AbstractRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected static string TableName
        {
            get;
        }
        protected static PropertyInfo PrimaryKey
        {
            get;
        }

        protected static PropertyInfo[] Others
        {
            get;
        }

        protected static string SelectAll
        {
            get;
            private set;           
        }

        protected static string SelectOne
        {
            get;
            private set;
        }

        protected static string Insert
        {
            get;
            private set;
        }

        protected static string Update
        {
            get;
            private set;
        }

        protected static string Delete
        {
            get;
            private set;
        }

        private static void CreateSelectAll()
        {
            SelectAll = $"SELECT *FROM [DBO].[{TableName}] WITH (NOLOCK)";

            SelectOne = $"SELECT *FROM [DBO].[{TableName}] WITH (NOLOCK) WHERE [{PrimaryKey.Name}] = @{PrimaryKey.Name}";
        }

        private static void CreateInsert()
        {
            var builder = new StringBuilder($"SELECT TOP 0 [id] INTO #Output FROM [dbo].[{TableName}]");

            builder.AppendLine($"INSERT INTO [DBO].[{TableName}] ({string.Join(",", Others.Select(x => $"[{x.Name}]"))})");
            builder.AppendLine("OUTPUT INSERTED.ID INTO #Output(ID)");
            builder.AppendLine($"VALUES ({string.Join(", ", Others.Select(x => $"@{ x.Name}"))})");

            builder.AppendLine("SELECT [id] FROM #Output");

            builder.AppendLine("DROP TABLE #Output");

            Insert = builder.ToString();
        }

        private static void CreateUpdate()
        {
            var builder = new StringBuilder();

            builder.AppendLine($"UPDATE [DBO].[{TableName}] ");           
            builder.AppendLine($"SET {string.Join(", ", Others.Select(x => $"[{x.Name}] = @{ x.Name}"))}");
            builder.AppendLine($"WHERE [{PrimaryKey.Name}] = @{PrimaryKey.Name}");
            Update = builder.ToString();
        }

        private static void CreatedDelete()
        {
            Delete = $"DELETE FROM [DBO].[{TableName}] WHERE [{PrimaryKey.Name}] = @{PrimaryKey.Name}";
        }

        public async Task<ICollection<T>> GetAllRecords(Func<T> factory)
        {
            var list= await SelectRecords(factory, SelectAll);
            return list;
        }

        public async Task<T> GetRecord(Func<T> factory,object key)
        {
            var list = await SelectRecords(factory, SelectOne, key);
            if (list.Count == 0)
                throw new RecordNotFoundException($"No data for the key {key}");

            return list[0];
        }

        public async Task<List<T>> SelectRecords(Func<T> factory,string sql,object key = null)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand(sql, connection))
                {
                    if (key != null)
                    {
                        command.Parameters.AddWithValue($"@{PrimaryKey.Name}", key);
                    }

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        var collection = new List<T>();

                        while (await reader.ReadAsync())
                        {
                            var row = factory();        

                            foreach (var prop in Others)
                            {
                                prop.SetValue(row, reader[prop.Name] == DBNull.Value ? null : reader[prop.Name]);
                            }

                            PrimaryKey.SetValue(row, reader[PrimaryKey.Name]);
                            collection.Add(row);

                            if (key != null)
                            {
                                return collection;
                            }
                            
                        }

                        return collection;
                    }
                }
            }
        }

        public async Task<T> InsertRecord(T row)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand(Insert, connection))
                {
                    foreach (var prop in Others)
                    {
                        command.Parameters.AddWithValue($"@{prop.Name}", prop.GetValue(row) ?? DBNull.Value);
                    }

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        var collection = new List<T>();

                        await reader.ReadAsync();                       

                        PrimaryKey.SetValue(row, reader[PrimaryKey.Name]);

                        return row;
                    }
                }
            }
        }

        public async Task<int> UpdateRecord(T row)
        {
            using (var connection = new SqlConnection(_connectionString))
            {      
                await connection.OpenAsync();

                using (var command = new SqlCommand(Update, connection))
                {
                    foreach (var prop in Others)
                    {
                        command.Parameters.AddWithValue($"@{prop.Name}",prop.GetValue(row) ?? DBNull.Value);
                    }

                    command.Parameters.AddWithValue($"@{PrimaryKey.Name}", PrimaryKey.GetValue(row));

                    return await command.ExecuteNonQueryAsync();
                }
            }
        }

        public async Task<int> DeleteRecord<TKey>(TKey key)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();

                using (var command = new SqlCommand(Delete, connection))
                {                    
                    command.Parameters.AddWithValue($"@{PrimaryKey.Name}", key);

                    return await command.ExecuteNonQueryAsync();
                }
            }
        }
    }
}
