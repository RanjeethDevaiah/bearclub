﻿using BearClub.DTO;
using BearClub.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BearClub.Repository
{
    /// <summary>
    /// This  repository assumes that all primary keys are named as id and are of Guid kind.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BearsClubRepository:AbstractRepository<Bear>, IBearsRepository
    {        
        public BearsClubRepository(IConfigurationRoot configuration)
            :base(configuration.GetConnectionString("Storage"))
        {

        }

        public Task<Bear> AddBear(Bear bear)
        {
            return InsertRecord(bear);
        }

        public Task<int> DeleteBear(Guid id)
        {
            return DeleteRecord(id);
        }

        public Task<ICollection<Bear>> GetAllBears()
        {
            return GetAllRecords(() => new Bear());
        }

        public Task<Bear> GetBear(Guid id)
        {
            return GetRecord(() => new Bear(), id);
        }

        public Task<int> UpdateBear(Bear bear)
        {
            return UpdateRecord(bear);
        }
    }
}
