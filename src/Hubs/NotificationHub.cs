﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace BearClub.Hubs
{
    [Authorize]
    public class NotificationHub:Hub
    {
    }
}
